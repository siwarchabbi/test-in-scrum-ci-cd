package org.example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Date d1=new Date();
        Date d2=new Date();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Task Details:");
        System.out.print("Enter Task ID: ");
        int taskId = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Enter Task Name: ");
        String taskName = scanner.nextLine();
        System.out.print("Enter Task Description: ");
        String taskDescription = scanner.nextLine();
        System.out.print("Enter Start Date (yyyy-MM-dd): ");
        String startDateString = scanner.nextLine();
        Date startDate = parseDate(startDateString);
        System.out.print("Enter Finish Date (yyyy-MM-dd): ");
        String finishDateString = scanner.nextLine();
        Date finishDate = parseDate(finishDateString);
        Task newTask = new Task(taskId, taskName, taskDescription, startDate, finishDate);
        ArrayList<Task> ListTasks=new ArrayList<>();
        ListTasks.add(newTask);
        for (Task task:ListTasks
        ) {
            System.out.println(task);
            System.out.println("TasK Add With Success ");
        }
    }
    private static Date parseDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            System.out.println("Invalid date format. Please enter date in yyyy-MM-dd format.");
            return null;
        }
    }
}
