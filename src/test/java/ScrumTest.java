import org.example.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScrumTest {
    @Test
    public void addTask() {
        Date startDate1 = new Date();
        Date finishDate1 = new Date();
        Task task1 = new Task(3, "Task 2", "Description 2", startDate1, finishDate1);
        ArrayList<Task> listTasks=new ArrayList<>();
        listTasks.add(task1);
        assertEquals(1,listTasks.size());
    }
}
